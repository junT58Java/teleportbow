package org.bitbucket.nico_harumaking.TeleportBowPlugin;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class TeleportBowCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if(args.length == 0){
			if(sender instanceof Player){
				Player p = (Player) sender;
				ItemStack bow = new ItemStack(Material.BOW);
				ItemMeta bowMeta = bow.getItemMeta();
				List<String> lore = new ArrayList<String>();
				lore.add(ChatColor.GRAY + "矢を消費してエンダーパールを放出します");
				bowMeta.setDisplayName(ChatColor.BLUE + "Teleport Bow");
				bowMeta.setLore(lore);
				bowMeta.addEnchant(Enchantment.ARROW_INFINITE, 2, true);
				bow.setItemMeta(bowMeta);
				p.getInventory().addItem(bow);
				p.updateInventory();
				sender.sendMessage(TeleportBowPlugin.getPrefix() + ChatColor.GOLD + "あなたにTeleportBowを配布しました。");
			}else{
				sender.sendMessage(TeleportBowPlugin.getPrefix() + ChatColor.RED + "使用方法: /" + label + " <player>");
			}
		}else{
			Player p = Bukkit.getPlayerExact(args[0]);
			if(p == null){
				sender.sendMessage(TeleportBowPlugin.getPrefix() + ChatColor.RED + args[0] + "はオンラインではありません！");
				return true;
			}
			ItemStack bow = new ItemStack(Material.BOW);
			ItemMeta bowMeta = bow.getItemMeta();
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "矢を消費してエンダーパールを放出します");
			bowMeta.setDisplayName(ChatColor.BLUE + "Teleport Bow");
			bowMeta.setLore(lore);
			bowMeta.addEnchant(Enchantment.ARROW_INFINITE, 2, true);
			bow.setItemMeta(bowMeta);
			p.getInventory().addItem(bow);
			p.updateInventory();
			sender.sendMessage(TeleportBowPlugin.getPrefix() + ChatColor.GOLD + args[0] + "にTeleportBowを配布しました。");
		}
		return true;
	}

}
