package org.bitbucket.nico_harumaking.TeleportBowPlugin;

import org.bukkit.ChatColor;
import org.bukkit.plugin.java.JavaPlugin;

public class TeleportBowPlugin extends JavaPlugin {

	public static String getPrefix(){
		return ChatColor.YELLOW + "[" + ChatColor.AQUA + "TeleportBow" + ChatColor.YELLOW + "]" + ChatColor.RESET;
	}

	public void onEnable() {
		getLogger().info(getDescription().getName() + "プラグイン(Ver:" + getDescription().getVersion() + ") が有効になりました。");
		TeleportBowRecipe recipes = new TeleportBowRecipe();
		getServer().addRecipe(recipes.TeleportBow());
		getServer().getPluginManager().registerEvents(new TeleportBowListener(), this);
		getCommand("teleportbow").setExecutor(new TeleportBowCommand());
	}

	public void onDisable() {
		getLogger().info(getDescription().getName() + "プラグイン(Ver:" + getDescription().getVersion() + ") が無効になりました。");
	}
}
