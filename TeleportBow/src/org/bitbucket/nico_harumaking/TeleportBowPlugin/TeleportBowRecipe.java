package org.bitbucket.nico_harumaking.TeleportBowPlugin;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

public class TeleportBowRecipe {
	public ShapedRecipe TeleportBow() {
		ItemStack bow = new ItemStack(Material.BOW);
		ItemMeta bowMeta = bow.getItemMeta();
		List<String> lore = new ArrayList<String>();
		lore.add(ChatColor.GRAY + "矢を消費してエンダーパールを放出します");
		bowMeta.setDisplayName(ChatColor.BLUE + "Teleport Bow");
		bowMeta.setLore(lore);
		bowMeta.addEnchant(Enchantment.ARROW_INFINITE, 2, true);
		bow.setItemMeta(bowMeta);
		ShapedRecipe recipe = new ShapedRecipe(bow);
		recipe.shape(new String[] { "EBD", "EDB", "EBD" });
		recipe.setIngredient('E', Material.ENDER_PEARL);
		recipe.setIngredient('B', Material.BOW);
		recipe.setIngredient('D', Material.DIAMOND);
		return recipe;
	}
}
