package org.bitbucket.nico_harumaking.TeleportBowPlugin;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.inventory.ItemStack;

public class TeleportBowListener implements Listener {

	@EventHandler
	public void onShootBow(EntityShootBowEvent event) {
		if ((event.getEntity() instanceof Player)) {
			Player player = (Player) event.getEntity();
			if (event.getBow().getEnchantmentLevel(Enchantment.ARROW_INFINITE) == 2) {
				if (!player.hasPermission("tpbow.use")) {
					player.sendMessage(ChatColor.RED + "You don't have permission to do this.");
					event.setCancelled(true);
					return;
				}else{
					if(player.getGameMode() != GameMode.CREATIVE){
						if (player.getInventory().contains(Material.ARROW)) {
							event.setCancelled(true);
							player.getInventory().removeItem(new ItemStack[] { new ItemStack(Material.ARROW, 1) });
							((EnderPearl) player.launchProjectile(EnderPearl.class)).setVelocity(event.getProjectile().getVelocity().multiply(1.1D));
							player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0F, 1.0F);
						} else {
							event.setCancelled(true);
						}
					}else{
						event.setCancelled(true);
						((EnderPearl) player.launchProjectile(EnderPearl.class)).setVelocity(event.getProjectile().getVelocity().multiply(1.1D));
						player.playSound(player.getLocation(), Sound.ENDERMAN_TELEPORT, 1.0F, 1.0F);
					}
				}
			}
		}
	}
}
